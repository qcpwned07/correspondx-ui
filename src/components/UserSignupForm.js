"use strict";

import React from 'react';
import { withRouter } from 'react-router-dom';
import Page from "./Page";

import { Card, Button, TextField } from '@material-ui/core';
import {  FormGroup, FormControl,  } from "react-bootstrap";
import {makeStyles} from "@material-ui/styles";
//import { AlertMessage } from './AlertMessage';
//import Page from './Page';


const style = {
    maxWidth: "500px",
    padding: "10px",
    margin: "auto",
};


class UserSignupForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username : '',
            password : '',
            email : '',
            firstname : '',
            lastname : '',
        };

        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeFirstname = this.handleChangeFirstname.bind(this);
        this.handleChangeLastname = this.handleChangeLastname.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChangeUsername(value) {
        this.setState({username: value});
    }
    
    handleChangePassword(value) {
        this.setState({password: value});
    }
    
    handleChangeEmail(value) {
        this.setState({email: value});
    }
    
    handleChangeFirstname(value) {
        this.setState({firstname: value});
    }
    
    handleChangeLastname(value) {
        this.setState({lastname: value});
    }
    
    
    handleSubmit(event) {
        event.preventDefault();

        let user = {
            username: this.state.username,
            password: this.state.password,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            email: this.state.email,
        };

        this.props.onSubmit(user);
    }

    render() {
        return (
            <Card style={style} className="md-block-centered">
                <form className="md-grid" onSubmit={this.handleSubmit} onReset={() => this.props.history.goBack()}>
                    <h4> Register </h4>
                    <TextField
                        label="Email"
                        id="emailField"
                        type="email"
                        className="col-md-12"
                        required={true}
                        value={this.state.email}
                        onChange={this.handleChangeEmail}
                        errorText="Username is required"/>
                    <p></p>
                    <TextField
                        label="Firstname"
                        id="firstnameField"
                        type="email"
                        className="col-md-6"
                        required={true}
                        value={this.state.email}
                        onChange={this.handleChangeFirstname}
                        errorText="Firstname is required"/>
                    <p></p>
                    <TextField
                        label="Last Name"
                        id="UsernameField"
                        type="text"
                        className="col-md-6"
                        required={true}
                        value={this.state.lastname}
                        onChange={this.handleChangeLastname}
                        errorText="Last name is required"/>
                    <p></p>
                    <TextField
                        label="Username"
                        id="UsernameField"
                        type="text"
                        className="md-row"
                        required={true}
                        value={this.state.username}
                        onChange={this.handleChangeUsername}
                        errorText="Username is required"/>
                    <p></p>
                    <TextField
                        label="Password"
                        id="PasswordField"
                        type="password"
                        className="md-row"
                        required={true}
                        value={this.state.password}
                        onChange={this.handleChangePassword}
                        errorText="Password is required"/>
                    <p></p>
                    <Button id="submit" type="submit"
                            disabled={this.state.username == undefined || this.state.username == '' || this.state.password == undefined || this.state.password == '' ? true : false}
                            raised primary className="md-cell md-cell--2">Register</Button>
                    <Button id="reset" type="reset" raised secondary className="md-cell md-cell--2">Dismiss</Button>
                </form>
            </Card>
        );
    }
}

export default withRouter(UserSignupForm);