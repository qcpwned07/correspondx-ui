import React from 'react'
import NavDropdown from "react-bootstrap/NavDropdown";
import UserService from '../services/UserService'
import UserLoginView from "../views/UserLoginView";

import {AccountCircle} from "@material-ui/icons";
import {IconButton, Card, Button, } from "@material-ui/core";
import UserLogin from "./UserLoginForm";
import {Nav} from "react-bootstrap"


const buttonStyle = {
    color: "#9EB2C9",
    border:"solid 1px",
    marginRight: "10px",
    position: "relative",
    float: "right",
    paading: "5px",
    borderRadius: "2px"


}

const loginCardStyle ={
    minWidth:"150px",
    minHeight:"150px"
}




export default class ProfileButton extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
        
        this.AccountButton = AccountButton.bind()
     }
     
     
     render() {
           return (
               <div>
                   {typeof this.state.user == 'undefined' ?
                       <div>
                           <Nav.Link href="/login" style={buttonStyle}>Login</Nav.Link>
                           <Nav.Link href="/register" style={buttonStyle}>Register</Nav.Link>
                       </div>
                       :
                       <NavDropdown title={<AccountButton/>} id="collapsible-nav-dropdown">
                           <NavDropdown.Item>   PUT USERNAME    </NavDropdown.Item>
                           <NavDropdown.Item>   LOGOUT          </NavDropdown.Item>
                       </NavDropdown>
                   }
               </div>

           );
     }
}

export function AccountButton (props) {
    return (
        <IconButton
            aria-label="Account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            color="inherit"
            float="right"
        >
            <AccountCircle/>
        </IconButton>
    );
}