"use strict";
import React from 'react';
import { styled } from '@material-ui/styles';
import MenuIcon from '@material-ui/icons/Menu';
import {  AppBar, Typography, Button, Toolbar, IconButton} from "@material-ui/core";
import { AccountCircle, } from "@material-ui/icons";
import { Nav, Navbar, NavItem, NavDropdown } from 'react-bootstrap'
import UserService from '../services/UserService'

import '../App.css'
import ProfileButton from "./ProfileButton";
import {Link, withRouter, SimpleLink} from "react-router-dom";

const styleAppBar = {
    background: 'linear-gradient(75deg, #3E6694 30%, #3E6694 90%)',
    border: 0,
    color: 'white',
    padding: '0 30px',
}

class TopBar extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            user: UserService.isAuthenticated() ? UserService.getCurrentUser() : undefined
        }
        this.handleMenu = this.handleMenu.bind(this)
    }

    handleMenu(event) {

    }

    /*
    logout() {
        UserService.logout();
        this.state = {
            user: UserService.isAuthenticated() ? UserService.getCurrentUser() : undefined
        };
        if(this.props.description.pathname != '/') {
            this.props.history.push('/');
        }
        else {
            window.description.reload();
        }
    }
     */

    render(){
        return(
            <AppBar style={ styleAppBar } position="fixed" backgroundColor="black">
                <Toolbar>
            <Navbar>
                <Navbar collapseOnSelect expand="lg" bg="#3e6694" variant="dark">
                    <IconButton edge="start" className="iconButton" color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Link to={'/'}>
                        <Navbar.Brand  onClick={ () => this.props.history.push('/') } >
                                <h3>Correspondx</h3>
                        </Navbar.Brand>
                    </Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                        </Nav>
                        <Nav>
                            <ProfileButton/>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>


                </Navbar>
                    </Toolbar>
                </AppBar>
        );
    }
}

export default withRouter(TopBar);