import React, { Component } from 'react';
import ChatBoxItem from './ChatBoxItem'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button'

import { Card, CardTitle, CardText } from '@material-ui/core';

class ChatBox extends Component{
  render() {
     
    return this.props.chatBox.map((message) => (
        <div class="ItemStyle">
            <ChatBoxItem message={message} />
        </div>
    ));
  }
} 



export default ChatBox;