import React, { Component } from 'react';
import ChatBoxItem from './ChatBoxItem'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button'
import ChatBox from "./ChatBox"
import { Card, CardTitle, CardText } from '@material-ui/core';


const cardStyle = {width:'100%', margin:'5px 15px', overflow:"hidden", display:"table", paddingTop:"5px 15px"}

class ConversationPage extends Component{
    state = {
        //Change for the recent convos lists name and correspongin img
        convos: [],
        messages: [],
    }

    constructor(props) {
        super(props);
    }

  render() {
      return (
            <div className="ConversationBox">

              <div className="ConvoTitle">
                <h2>Letters to my wife</h2>
              </div>

              {/* let PublicButton = this.state.PublicBtn.black ? "blackButton" : "whiteButton";  */}
              {/* <div className={ `publicButton` }> */}
                <Button variant="info" size="large" className="PublicButton" onClick="this.changeColor.bind(this)" variant="outlined" > Public </Button>
              {/* </div> */}

              <Card className="ChatBox">
                <ChatBox chatBox={this.props.messages} />
              </Card>

              <Card className="TextBox">
                <input type="text" maxlength="4"  placeholder="Write your important message " />
              </Card>

            </div>
      );
  }
} 



export default ConversationPage;