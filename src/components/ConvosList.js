import React, { Component } from 'react';
import ConvoListItem from './ConvoListItem'

import PropTypes from 'prop-types'
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button'
import {
    Card, CardContent, List, ListItem,
    Divider,
    ListItemText,
    ListItemAvatar,
    Avatar,
    Typography, ListSubheader
} from '@material-ui/core';

class ConvosList extends Component{
    constructor(props) {
        super(props);
        this.state = {
            correspondances : []
        }
    }
    componentDidMount() {
        fetch('http://localhost:5000/correspondance')
            .then(res => res.json())
            .then((data) => {
                this.setState({ correspondances : data[0] })
            })
            .catch(console.log)
        console.log(this.state.correspondances[0])
    }

  render() {
     
    return (
        <Card>
            <List
                style={{backgroundColor:"white", color:"white"}}
                aria-labelledby="nested-list-subheader"
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                        OPEN CONVERSATIONS
                    </ListSubheader>
                }
            >
                {console.log(" Correspondances " + typeof this.state.correspondances[0])}
                {console.log(this.state.correspondances.title)}
                {console.log(Object.keys(this.state.correspondances))}
                {console.log(this.state.correspondances)}
                    { this.state.correspondances.map((convo) => ( <ConvoListItem convo={convo} /> )) }
            </List>
        </Card>
    );
  }
} 



export default ConvosList;