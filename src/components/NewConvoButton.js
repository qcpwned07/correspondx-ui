
import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import { Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

class NewConvoButton extends Component{
  render() {

    return (
        <Fab color="primary" aria-label="Add" className="AddNewConvoButton"
             style={{backgroundColor: "#3e6694",height: "48px", width: "48px",  }}>
            <AddIcon />
        </Fab>
    );
  }
}



export default NewConvoButton;

