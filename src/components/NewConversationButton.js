import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import BlogFormView from "../views/BlogFormView";
import {Redirect} from "react-router-dom";
import BlogService from '../services/BlogService';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {Fab} from "@material-ui/core";

const styleDiv = {display:"inline-block", textAlign:"right", float:"right", marginRight:"1%"}


function show () { this.setState({visible:true})};
function hide () { this.setState({visible:false})};
function renderRedirect() { this.setState({redirect:true})};

export class NewConversationButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible:false,
            redirect:false
        };
        this.show = show.bind(this);
        this.hide = hide.bind(this);
        this.renderRedirect = renderRedirect.bind(this)
    };


    render() {
        const {visible} = this.state.visible;
        const actions = [{
            onClick: this.hide,
            primary: true,
            children: 'Cancel',
        }];

        const tooltipLabel = this.props.active ? "Create a new conversation" : "Login to create a conversation with a friend"
        return (
            <div>
                {this.state.redirect /* && <Redirect to="/login"/>*/}
                <div style={styleDiv}>
                    <Fab
                        tooltipLabel={tooltipLabel}
                        className="addButton"
                        onClick={this.props.active ? this.show : this.renderRedirect}>
                        <AddIcon/>
                    </Fab>
                </div>
                <Dialog
                    visible={visible}
                    id="createBlogPostDialog"
                    onHide={this.hide}
                    modal
                    actions = {actions}
                    dialogStyle={{width:"90%"}}>
                    <BlogFormView/>
                </Dialog>

            </div>
        );
    }
}
export class EditButton extends React.Component {

    constructor(props) {
        super(props)

        this.state = {visible: false};
        this.show = show.bind(this)
        this.hide = hide.bind(this)
        this.renderRedirect = renderRedirect.bind(this)
    };

    render() {
        const {visible} = this.state
        const actions = [{
            onClick: this.hide,
            primary: true,
            children: 'Cancel',
            },
            {onClick: () => {
                BlogService.updateBlog(this.props.post).then(
                    this.renderRedirect()
                )},
            primary: true,
            children: "Update"
            }
        ];

        return (
            <div>
                <div style={styleDiv}>
                    <Button floating primary tooltipLabel="Edit this Post" className="editButton" onClick={this.show}>
                        <EditIcon/>
                    </Button>
                </div>

                <Dialog
                    visible={visible}
                    id="createBlogPostDialog"
                    onHide={this.hide}
                    modal
                    actions = {actions}
                    dialogStyle={{width:"90%"}}>
                    <BlogFormView article={this.props.post}/>
                </Dialog>
            </div>
        );
    }
}

export class DeleteButton extends React.Component {
     constructor(props) {
        super(props)
        this.state = {
                visible: false,
                redirect: false,
            };
        this.show = show.bind(this)
        this.hide = hide.bind(this)
        this.renderRedirect = renderRedirect.bind(this)
    };



    render() {
        const {visible} = this.state
        const actions = [{
            onClick: this.hide,
            primary: true,
            children: 'Cancel',
            },
            {onClick: () => {
                BlogService.deleteBlog(this.props.id).then(
                   this.renderRedirect()
                )},
            primary : true,
            children: "Delete"
            }
        ];

        return (
            <div>
                <div style={styleDiv}>
                    {this.state.redirect && <Redirect to="/blog"/> }
                     <Button floating primary tooltipLabel="Delete this Post" className="deleteButton" onClick={this.show} >
                        <DeleteIcon />
                    </Button>
                </div>
                <Dialog
                    open={false}
                    visible={visible}
                    id="createBlogPostDialog"
                    onHide={this.hide}
                    modal
                    actions={actions}
                    dialogStyle={{width: "90%"}}>
                    Are you sure you want to delete this post?
                </Dialog>
            </div>
        );
    }
}


