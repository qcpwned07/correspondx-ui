"use strict";

import React from 'react';

import TopBar from './TopBar';
import NewConvoButton from "./NewConvoButton";
import Card from "@material-ui/core/Card";
import ConvosList from "./ConvosList";

export default class Page extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            convos : [],
            username : "Matth",
        }
    }
    
    render() {
        return (
            <section>
                <TopBar/>
                <div style={{margin:"2%"}}>
                <div style={{minHeight:"100px"}}> </div>
                
                <div class="NewConvoTopBar" style={{ display:"block", textAlign:"right"}}>
                    <NewConvoButton />
                </div>
    
                {typeof this.state.username === "undefined"
                    ? <div style={{minWidth:"20%"}}></div>
                    : <Card className="ConvosList">
                        <ConvosList convosList={this.props.convos}/>
                    </Card>
                }
                    
                {this.props.children}
            </div>
                
            </section>
        );
    }
}