import React, { Component } from 'react'
import './ConvoListItem.css'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import {Card, CardContent, List, ListItem,
    Divider,
    ListItemText,
    ListItemAvatar,
    Avatar,
    Typography } from '@material-ui/core';
import {Link} from "react-router-dom";


export class ConvoListItem extends Component {

    constructor(props){
        super(props);

        this.handleListItemClick = this.handleListItemClick.bind()
    }

    handleListItemClick(event, index) {
    }

    render() {
        return (
            <Link to={ `conversation/` + this.props.convo.id }>
                <ListItem dense button alignItems="flex-start" onClick={event => this.handleListItemClick(event, 1) } >
                    <ListItemAvatar>
                        <Avatar alt="Remy Sharp" src={this.props.convo.imgPath} />
                    </ListItemAvatar>
                    <ListItemText
                        primary= { this.props.convo.title}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    color="textPrimary"
                                >
                                    { this.props.convo.user_id_01 }
                                </Typography>
                                { this.props.convo.description}
                            </React.Fragment>
                        }
                    />
                    <div className="itemStyle" >
                    </div>

                </ListItem>
                <Divider variant="inset" component="li" />
            </Link>

        )
    }
} 





export default ConvoListItem
