"use strict";

import React from 'react';
import { Card, Button, TextField } from '@material-ui/core';
import { withRouter, Link } from 'react-router-dom';
import {  FormGroup, FormControl,  } from "react-bootstrap";
import {makeStyles} from "@material-ui/styles";


//import { AlertMessage } from './AlertMessage';
//import Page from './Page';


const style = {
    margin: "auto",
    width: "50%",
    padding: "10px",
    maxWidth: "500px"
};


class UserLoginForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username : '',
            password : ''
        };

        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateForm = this.validateForm.bind(this);
    }

    handleChangeUsername(value) {
        this.setState({username: value});
    }

    handleChangePassword(value) {
        this.setState({password: value});
    }
    
    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }
    
    
    
    handleSubmit(event) {
        event.preventDefault();

        let user = {
            username: this.state.username,
            password: this.state.password
        };

        this.props.onSubmit(user);
    }

    render() {
        return (
            <Card style={style} className="md-block-centered">
                <form className=" " onSubmit={this.handleSubmit} onReset={() => this.props.history.goBack()}>
                    <TextField
                        label="Login"
                        id="LoginField"
                        controlId="username"
                        type="text"
                        className="col-md-12  centered"
                        required={true}
                        //value={this.state.username}
                        placeholder="Username"
                        onChange={this.handleChangeUsername}
                        errorText="Login is required"
                        variant="outlined"/>
                    <p></p>
                    <TextField
                        label="Password"
                        id="PasswordField"
                        type="password"
                        className="md-row"
                        required={true}
                        placeholder="Password"
                        style={{margin:"auto"}}
                        className="col-md-12  centered"
                        //value={this.state.password}
                        onChange={this.handleChangePassword}
                        variant="outlined"
                        errorText="Password is required"/>
                    <p></p>

                    <Button id="submit" type="submit"
                            disabled={this.state.username == undefined || this.state.username == '' || this.state.password == undefined || this.state.password == '' ? true : false}
                            raised primary className="md-cell md-cell--2">Login</Button>
                    <Button id="reset" type="reset" raised secondary className="md-cell md-cell--2">Dismiss</Button>
                    <Link to={'/register'} className="md-cell">Not registered yet?</Link>
                </form>
            </Card>
        );
    }
}

export default withRouter(UserLoginForm);

//<AlertMessage className="md-row md-full-width" >{this.props.error ? `${this.props.error}` : ''}</AlertMessage>
