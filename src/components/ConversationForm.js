"use strict";

import React from 'react';
import { Button, TextField } from 'react-md';
import { withRouter } from 'react-router-dom';
import UserService from '../services/UserService';
import { AlertMessage } from './AlertMessage';
import BlogService from "../services/BlogService";




class ConversationForm extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.conversation != undefined) {
            this.state = {
                title : props.conversation.title,
                description : props.conversation.description,
                date : props.conversation.date,
                content : props.conversation.content,
                authorID: props.conversation.authorID,
                authorUsername: props.conversation.authorUsername,
            };
        } else {
            this.state = {
                title : '',
                description : '',
                date : '',
                content : '',
                authorID : '',
                authorUsername: '',
                img: ''
            };
        }

        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleChangeImg = this.handleChangeImg.bind(this);
        this.updateBlog = this.updateBlog.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    updateBlog(article) {
        if(this.state.article == undefined) {
            BlogService.createBlog(article).then((data) => {
                this.props.history.push('/');
            }).catch((e) => {
                console.error(e);
                this.setState(Object.assign({}, this.state, {error: 'Error while creating blog'}));
            });
        } else {
            BlogService.updateBlog(article).then((data) => {
                this.props.history.goBack();
            }).catch((e) => {
                console.error(e);
                this.setState(Object.assign({}, this.state, {error: 'Error while updating blog'}));
            });
        }
    }

    handleChangeTitle(value) {
        this.setState(Object.assign({}, this.state, {title: value}));
    }

    handleChangeDescription(value) {
        this.setState(Object.assign({}, this.state, {description: value}));
    }

    handleChangeDate(value) {
        this.setState(Object.assign({}, this.state, {date: value}));
    }

    handleChangeContent(value) {
        this.setState(Object.assign({}, this.state, {content: value}));
    }

    handleChangeImg(value) {
        this.setState(Object.assign({}, this.state, {img: value}));
    }

    handleSubmit(submitArticle) {
        submitArticle.preventDefault();

        let article = this.props.article;
        if(article == undefined) {
            article = {};
        }

        article.title = this.state.title;
        article.description = this.state.description;
        article.date = this.state.date;
        article.content = this.state.content;
        article.authorID = UserService.getCurrentUser().id;
        article.authorUsername = UserService.getCurrentUser().username;
        article.img = this.state.img;

        this.props.onSubmit(article);
    }


    render() {
        return (
                <form className="md-grid" onSubmit={this.handleSubmit} onReset={() => this.props.history.goBack()}>
                    <TextField
                        label="Post title"
                        id="TitleField"
                        type="text"
                        className="md-row"
                        required={true}
                        value={this.state.title}
                        onChange={this.handleChangeTitle}
                        errorText="Title is required"/>
                    <TextField
                        label="Location associated with this post (if not applicable, leave blank)"
                        id="LocationField"
                        type="text"
                        className="md-row"
                        required={true}
                        value={this.state.description}
                        onChange={this.handleChangeDescription}
                        errorText="Location is required"/>
                    <TextField
                        label="Date"
                        id="DateField"
                        type="text"
                        className="md-row"
                        required={true}
                        value={this.state.date}
                        onChange={this.handleChangeDate}
                        errorText="Date is required"/>
                    <TextField
                        label="Content"
                        id="ContentField"
                        type="text"
                        className="md-row"
                        rows={5}
                        required={false}
                        value={this.state.content}
                        onChange={this.handleChangeContent}/>
                    <Button id="submit" type="submit"
                            disabled={this.state.title == undefined || this.state.title == '' || this.state.date == undefined || this.state.date == '' }
                            raised primary className="md-cell md-cell--2">Submit</Button>
                    <AlertMessage className="md-row md-full-width" >{this.props.error ? `${this.props.error}` : ''}</AlertMessage>
                </form>
        );
    }
}

export default withRouter(ConversationForm);
