import React, { Component } from 'react'
import './ChatBoxItem.css'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import {Card} from 'react-md'


export class ChatBoxItem extends Component {
    render() {
        return (
              this.props.message.sent == true
              ?
              <div className="itemStyleSender" >
              <Card style={{ padding:"15px 15px 0"}}>
                <p>{ this.props.message.content } </p>
              </Card>
              </div>
              
                
            : (
            <div className="itemStyleReceiver" >
            <Card style={{ padding:"15px 15px 0"}}>
               <p>{ this.props.message.content } </p>
            </Card>
            </div>
            )
            
        )
    }
} 

export default ChatBoxItem