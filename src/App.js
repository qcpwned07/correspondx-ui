import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import ConvosList from './components/ConvosList'
import ChatBox from './components/ChatBox'
import './App.css'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import NavigationIcon from '@material-ui/icons/Navigation';
import TopBar from "./components/TopBar";
import NewConvoButton from "./components/NewConvoButton"
import ConversationPage from "./components/ConversationPage";
import {Card, CardTitle, CardText, Slider} from '@material-ui/core';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Page from "./components/Page";
import ConvoListView from "./views/ConvoListView"
import UserLoginView from "./views/UserLoginView";
import ConversationView from "./views/ConversationView";
import UserSignupView from "./views/UserSignupView";



const style = { maxWidth: 320 };


class App extends Component {
    constructor (props) {
        super(props);
        this.state = {
            //Change for the recent convos lists name and correspongin img
            convos: [
                {
                    title: "Day at the beach",
                    name: 'Fayo',
                    imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
                },
                {
                    title: "Hegel v.s Marx",
                    name: 'Yush the Dealer',
                    imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
                },
                {
                    title: "Poems",
                    name: 'Matt Smilenot',
                    imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
                },
                {
                    title: "letters to my bro",
                    name: 'Dude man Bro',
                    imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
                },
                {
                    title: "How to be an asshole ",
                    name: 'Hans Morin',
                    imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
                }
            ],
            messages: [
                {
                    time: '12h51',
                    content: "Hi matt, how you doing?",
                    sent: false
                },
                {
                    time: '12h59',
                    content: "sup man, long time no see, I've been working a lot and I'm exausted, anyways...",
                    sent: true
                },
                {
                    time: '1h22',
                    content: "well I've been riding that boat I told you about... rides great!",
                    sent: false
                }
            ],
            routes: [
                {component: ConvoListView, path: "/", exact: true},
                {component: ConvoListView, path: "/conversations", exact: true},
                {component: ConversationView, path: '/conversation/:id' },
                //{ component: EventDetailView , path: '/participate/:id'},
                {component: UserLoginView, path: '/login'},
                {component: UserSignupView, path: '/register'},
                //{ component: UserSignupView, path: '/register'}
            ],
            PublicBtn: {
                black: true
            }
        }
    };

  // function to switch public button color wether the conversation is public or not
  changeColor(){
	this.setState({black: !this.state.black})
 }

  
  render() {
	console.log(this.state.convos)
 
	return (
	    <Router>
                <div className="App" >
    
                    <Switch>
                        {this.state.routes.map((route, i) => (<Route key={i} {...route}/>) )}
                    </Switch>

                </div>
        </Router>
	);
  }
}


export default App;

/*
<Route path="/" exact component={ConvoListView}/>
<Route path="/conversations/"  component={ConvoListView}/>
 */
