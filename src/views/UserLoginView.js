"use strict";

import React from 'react';

import UserLogin from '../components/UserLoginForm';

import UserService from '../services/UserService';
import Page from "../components/Page";


const style = {
    paddingTop:"10%",
    paddingBottom:"35%",
}

class UserLoginView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    login(user) {
        UserService.login(user.username, user.password).then((data) => {
            this.state.error ? console.error(this.state.error) :  this.props.history.goBack();
        }).catch((e) => {
            console.error(e);
            this.setState({
                error: e
            });
        });
    }

    render() {
        return (
            <Page>
                <div style={style}>
                <UserLogin onSubmit={(user) => this.login(user)} error={this.state.error}/>
                </div>
            </Page>
        );
    }
}

export default UserLoginView;