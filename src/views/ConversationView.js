import React from 'react'
import Page from "../components/Page";
import {Card} from "@material-ui/core"
import ConversationPage from "../components/ConversationPage";






export default class ConversationView extends React.Component {
   
   constructor (props) {
       super(props);
       this.state = {
           convos: [
               {
                   title: "Day at the beach",
                   name: 'Fayo',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
               },
               {
                   title: "Hegel v.s Marx",
                   name: 'Yush the Dealer',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
               },
               {
                   title: "Poems",
                   name: 'Matt Smilenot',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               },
               {
                   title: "letters to my bro",
                   name: 'Dude man Bro',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               },
               {
                   title: "How to be an asshole ",
                   name: 'Hans Morin',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               }
           ],
           messages: [
               {
                   time: '12h51',
                   content: "Hi matt, how you doing?",
                   sent: false
               },
               {
                   time: '12h59',
                   content: "sup man, long time no see, I've been working a lot and I'm exausted, anyways...",
                   sent: true
               },
               {
                   time: '1h22',
                   content: "well I've been riding that boat I told you about... rides great!",
                   sent: false
               }
           ],
       }
   }
   
   render() {
       return (
           <Page convos={this.state.convos}>
               <Card>
                   <ConversationPage messages={this.state.messages} />
               </Card>
           </Page>
       );
   }
}