"use strict";

import React, { Component} from 'react';
import BlogForm from '../components/ConversationForm';
import BlogService from '../services/BlogService';
import {Redirect} from "react-router-dom";

class BlogFormView extends Component {

    constructor(props) {
        super(props);
        this.state = {redirect: false}
    }

    render() {
        if (this.state.loading) {
            return (<h2>Loading...</h2>);
        } else if (this.state.redirect) {
            return (<Redirect to="/"/>);
        }

        return (
            <div className="modalOverlay">
                <h1>Create a new conversation</h1>
                <BlogForm article={this.state.article} onSubmit={(article) => this.updateArticle(article)}
                          error={this.state.error}/>
            </div>
        );
    }
}