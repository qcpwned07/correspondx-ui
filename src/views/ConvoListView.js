import React from 'react'
import Page from "../components/Page";






export default class ConvoListView extends React.Component {
   
   constructor (props) {
       super(props);
       this.state = {
           convos: [
               {
                   title: "Day at the beach",
                   name: 'Fayo',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
               },
               {
                   title: "Hegel v.s Marx",
                   name: 'Yush the Dealer',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
               },
               {
                   title: "Poems",
                   name: 'Matt Smilenot',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               },
               {
                   title: "letters to my bro",
                   name: 'Dude man Bro',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               },
               {
                   title: "How to be an asshole ",
                   name: 'Hans Morin',
                   imgPath: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcRjhLYEC0x8puwMvk6vkAWpSZK1sSIv2Xe1rBH1yTXfV26n4Eug'
            
               }
           ],
       }
   }
   
   render() {
       return (
           <Page convos={this.state.convos}>
           
           </Page>
       );
   }
}